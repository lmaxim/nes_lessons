.segment "HEADER"
    .byte $4E, $45, $53, $1A    ; NES\0
    .byte $02                   ; PRG 2x16 = 32kb
    .byte $01                   ; CHR 1x8  = 8kb
    .byte %00000000             ; NROM-256
    .byte %00000000
    
.segment "STARTUP"
reset:

    sei
    cld
    
    APU_FRAME_COUNTER   = $4017

    lda #%01000000                 ; irq inhibit flag
    sta APU_FRAME_COUNTER

    PPU_CTRL            = $2000        ; https://wiki.nesdev.com/w/index.php/PPU_registers
    PPU_MASK            = $2001
    APU_DMC_CTRL        = $4010        ; https://wiki.nesdev.com/w/index.php/APU_registers

    lda #%00000000
    sta PPU_CTRL
    sta PPU_MASK
    sta APU_DMC_CTRL

    PPU_STATUS          = $2002
    bit PPU_STATUS

@vblank1:
    bit PPU_STATUS
    bpl @vblank1

@vblank2:
    bit PPU_STATUS
    bpl @vblank2

    PPU_ADDR            = $2006
    PPU_DATA            = $2007

    lda #$3f
    sta PPU_ADDR                    ; адрес $3f00
    lda #$00
    sta PPU_ADDR

    ; https://wiki.nesdev.com/w/index.php/PPU_palettes

    ldx #$00                        
lbl_load_palette:
    lda palette, x
    sta PPU_DATA
    inx
    cpx #$20                        ; 4 * 4 bgr + 4 * 4 spr = 32
    bne lbl_load_palette            ; if x != 32 lbl_load_palette

    ; будем писать на экране нашими символами
    lda #$21
    sta PPU_ADDR
    lda #$ca
    sta PPU_ADDR

    ldx #$00                        
lbl_load_title:
    lda game_title, x
    sta PPU_DATA
    inx
    cpx #$0a                        ; if x != len('%game title%') 10 == $0a
    bne lbl_load_title              ; then lbl_load_title

    PPU_SCROLL          = $2005
    lda #$00
    sta PPU_SCROLL
    sta PPU_SCROLL

    LDA #%10010000                  ; enable NMI, sprites from Pattern Table 0
    STA PPU_CTRL

    PPU_MASK_SHOW_BGR   = %00001000

    LDA #PPU_MASK_SHOW_BGR
    STA PPU_MASK

    cli

main:
    jmp main

nmi:
    rti

irq:
    rti

.segment "RODATA"
palette:
    .byte $21, $30, $35, $3f    ; bgr 1  
    .byte $21, $13, $12, $11    ; bgr 2
    .byte $21, $23, $22, $21    ; bgr 3
    .byte $21, $33, $32, $31    ; bgr 4
    
    .byte $21, $35, $30, $3f    ; spr 1
    .byte $21, $35, $30, $3f    ; spr 2
    .byte $21, $35, $30, $3f    ; spr 3
    .byte $21, $3f, $35, $30    ; spr 4

press_start_message:
    .byte $19, $1b, $0e, $1c, $1c     ; PRESS
    .byte $00                         ; ' '
    .byte $1c, $1d, $0a, $1b, $1d     ; START

game_title:
    .byte $10, $0a, $16, $0e         ; GAME
    .byte $00                        ; ' '
    .byte $1d, $12, $1d, $15, $0e    ; TITLE

.segment "VECTORS"
    .word nmi                   ; nmi
    .word reset                 ; $0000, первая точка после запуска
    .word irq                   ; вторая точка после BRK

.segment "CHARS"
    .incbin "lesson02.chr"
