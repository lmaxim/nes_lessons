# Helloworld для бедных на денди

## tl;dr

```
ca65 lesson01.s && ld65 lesson01.o -t nes -m map.txt -vm -o lesson01.nes
wine ~/Downloads/fceux64/fceux64.exe lesson01.nes
```

## Шаги

- Установить cc65 чтобы были доступны ca65 ld65 - объяснить что это и для чего каждый нужен
- Далее сказать про то что есть у нас пустой файл

- Как запускать сборку, как проверить что вообще у нас что-то работает
-- Используем документацию
https://cc65.github.io/doc/ca65.html точнее https://cc65.github.io/doc/ca65.html#toc2.1

## Потребуется

- любой любимый/удобный редактор
- cc65 (ca65 + ld65)
- FCEUX
- HexFiend

### Запускаем 

[09:55:30]:src $ ca65 main.s 
[09:56:26]:src $ ls
main.o  main.s


[09:57:06]:src $ ld65 main.o 
ld65: Error: Memory configuration missing

https://cc65.github.io/doc/ld65.html#s5

Подскажем
https://cc65.github.io/doc/ld65.html#toc2.1 ключик -t

[10:00:13]:src $ ld65 -t nes main.o 
ld65: Warning: /usr/local/Cellar/cc65/2.18/share/cc65/cfg/nes.cfg(63): Segment 'HEADER' does not exist
ld65: Warning: /usr/local/Cellar/cc65/2.18/share/cc65/cfg/nes.cfg(63): Segment 'STARTUP' does not exist
ld65: Warning: /usr/local/Cellar/cc65/2.18/share/cc65/cfg/nes.cfg(63): Segment 'VECTORS' does not exist
ld65: Warning: /usr/local/Cellar/cc65/2.18/share/cc65/cfg/nes.cfg(63): Segment 'CHARS' does not exist

О нет! опять ругается, ну чтож, дополним наш main.s 
https://cc65.github.io/doc/ca65.html#ss11.96

```
.segment "HEADER"
.segment "STARTUP"
.segment "VECTORS"
.segment "CHARS"
```

Запускаем: ld65 -t nes main.o 
та же ошибка, но как так?!

А все потому что файлик-то .o используется из предыдущего шага, нужно его пересобрать

[10:04:29]:src $ ca65 main.s 
[10:04:34]:src $ ld65 -t nes main.o 
[10:05:37]:src $ 

нет ошибки!

Попробуем запустить FCEUX

[10:05:37]:src $ ls
a.out   main.o  main.s

у нас какой-то a.out а где же наша nes игра?!

ld65 -t nes main.o -o main.nes 
https://cc65.github.io/doc/ld65.html#toc2.1 
-o как в результате будет называться собранный файл

Файл теперь есть, но эмулятор ругается, почему?! 
Да потому что он не знает что это за ROM какой ему маппер нужно поддерживать, как в этом убедиться? Посмотреть в hex редакторе

Открываем, а там все по нулям. Ок, значит надо сказать эмулятору что это, через заголовок
https://wiki.nesdev.com/w/index.php/INES

```
The format of the header is as follows:

0-3: Constant $4E $45 $53 $1A ("NES" followed by MS-DOS end-of-file)
4: Size of PRG ROM in 16 KB units
5: Size of CHR ROM in 8 KB units (Value 0 means the board uses CHR RAM)
6: Flags 6 - Mapper, mirroring, battery, trainer
7: Flags 7 - Mapper, VS/Playchoice, NES 2.0
8: Flags 8 - PRG-RAM size (rarely used extension)
9: Flags 9 - TV system (rarely used extension)
10: Flags 10 - TV system, PRG-RAM presence (unofficial, rarely used extension)
11-15: Unused padding (should be filled with zero, but some rippers put their name across bytes 7-15)
```

+ то что нам досталось от компилятора/линковщика
default NES ROM layout (2x16k PRG, 8k CHR)


+ чтоб сейчас во всех этих мапперах не разбираться то 6 + 7: 
Flags 6 - Mapper, mirroring, battery, trainer
Flags 7 - Mapper, VS/Playchoice, NES 2.0
оставим по нулям, будет NROM https://wiki.nesdev.com/w/index.php/NROM

```
.segment "HEADER"
    .byte $4E, $45, $53, $1A    ; NES\0
    .byte $02                   ; PRGrom
    .byte $01                   ; CHRrom
    .byte $00                   ; MAPPER
    
    .byte $00                   ; 0-low(MAPPER) 0-mirroring, etc
    .byte $00                   ; 0-hight(MAPPER)

    ; остальное заполнит линковщик
```

### Собрали, запустилось но ничего нет, как так?!

А вот тут начинается магия, про которую надо придумать как рассказать просто

- Для начала посмотрим debugger и увидим, что все у нас зависло на первой строчке 
почему?
BRK по умолчанию вызвает прерывание 
http://www.thealmightyguru.com/Games/Hacking/Wiki/index.php?title=BRK

https://wiki.nesdev.com/w/index.php/CPU_interrupts
Normally, the NMI vector is at $FFFA, the reset vector at $FFFC, and the IRQ and BRK vector at $FFFE

Гипотеза:
NES не знает откуда начать выполнение (где адрес в памяти где начинается программа), но счетчик команд начинается с 0 и она пробует начать с 0000 адреса
Там встречает команду BRK которая отвечает за прерывание, возможно у нас где-то написано, что по какому-то адресу это прерывание нужно обработать, вот это и найдем

Как? Через линковщик - пусть скажет что и куда положил

https://cc65.github.io/doc/ld65.html#ss2.1
-m name               Create a map file

Т.к. код не меняли, давай как есть, скажи нам
ld65 -t nes main.o -m main.map -o main.nes

открываем main.map в текстовом редакторе, хреновенько и ничего не понятно
добавим -vm                   Verbose map file

ld65 -t nes main.o -m main.map -vm -o main.nes

Тут важное для нас 
```
CODE                  008000  008000  000000  00001
RODATA                008000  008000  000000  00001
STARTUP               008000  008000  000000  00001
VECTORS               00FFFA  00FFFA  000000  00001
```

Попробуем для векторов задать явно адреса которые тут понаписаны, точнее $8000, точнее давайте раскидаем на 8010 8020 8030 чтоб понять, какое событие нам все портит

https://wiki.nesdev.com/w/index.php?title=CPU_memory_map&redirect=no
Идут они в таком порядке

```
The CPU expects interrupt vectors in a fixed place at the end of the cartridge space:

$FFFA-$FFFB = NMI vector
$FFFC-$FFFD = Reset vector
$FFFE-$FFFF = IRQ/BRK vector
```

Получается

```
.segment "VECTORS"
    .word $8010                 ; nmi
    .word $8020                 ; reset
    .word $8030                 ; irq
```


??? Также на объяснить про то что irq это не только brk но и прерывания от APU аудио


Собираем что у нас получилось
```
[11:00:21]:src $ ca65 main.s 
[11:10:13]:src $ ld65 -t nes main.o -m main.map -vm -o main.nes
```

Ставим дебагер на все пространство от 0000-FFFF
сперва случилось 8020 
потом 8030
и далее 8030 .... 8030 т.к. нет деления на приоритеты, то NMI прерывание хоть и случается но управления не получает

# заведем reset
в отличие от прерываний, хоть оно им и является но делать у него rti не нужно, тк не ясно, а куда возвращаться

но следом сделать бесконечный цикл - нормально

Сделаем sei поставим флаг, что мы находимся в прерывании, чтобы не сорваться на выполнение прерывания по BRK / IRQ
В конце уберем флаг, поставив cli (это к вопросу почему мы не используем rti)



# Расширить файл 
- Рано
+ Пока не много можно

```
ca65 main.s
ca65 nmi.s

ld65 -t nes main.o nmi.o -m main.map -vm -o main.nes
```

**nmi.s**
```
.export nmi

.segment "CODE"

nmi:
    rti
```

**main.s**
```
.import nmi

.segment "VECTORS"
    .word nmi                   ; nmi
```

## Hello world для бедных

У nes нет знакогенератора, но есть звукогенератор. Воспользуемся им
https://wiki.nesdev.com/w/index.php/APU

## Ок, а что с графикой?

Пока ничего, тема следующего урока