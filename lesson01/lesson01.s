.segment "HEADER"
    .byte $4E, $45, $53, $1A    ; NES\0
    .byte $02                   ; PRG 2x16 = 32kb
    .byte $01                   ; CHR 1x8  = 8kb
    .byte %00000000             ; NROM-256
    .byte %00000000
    
.segment "STARTUP"
reset:

    lda #%00000100
    sta $4015

    lda #%10000001              ; счетчик выключили, включили громкость
    sta $4008
    
    lda #%00100010              ; подобрали частоту чтобы получить ноту
    sta $400A

    lda #%00000000
    sta $400B

main:
    jmp main

nmi:
    rti

irq:
    rti


.segment "VECTORS"
    .word nmi                   ; nmi
    .word reset                 ; $0000, первая точка после запуска
    .word irq                   ; вторая точка после BRK
.segment "CHARS"