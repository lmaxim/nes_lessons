.segment "HEADER"
    .byte $4E, $45, $53, $1A    ; NES\0
    .byte $02                   ; PRG 2x16 = 32kb
    .byte $01                   ; CHR 1x8  = 8kb
    .byte %00000000             ; NROM-256
    .byte %00000000
    
.segment "STARTUP"
reset:
    sei
    cld

    PPUCTRL = $2000
    PPUMASK = $2001
    PPUSTATUS = $2002
    PPUSCROLL = $2005
    PPUADDR = $2006
    PPUDATA = $2007

    bit PPUSTATUS
vblank1:
    bit PPUSTATUS
    bpl vblank1

vblank2:
    bit PPUSTATUS
    bpl vblank2

    lda #%00010000
    sta PPUCTRL

    lda #%00001000
    sta PPUMASK

    lda #$3f
    sta PPUADDR
    lda #$00
    sta PPUADDR

    ldx $00                 ; 0->10 ; $00->$04
palette_loop:
    lda palette, x
    sta PPUDATA
    inx
    cpx #$04
    bne palette_loop


;;;;;;;;;;;;;;;;;;;;;;;;
    lda #$21
    sta PPUADDR
    lda #$cb
    sta PPUADDR

    ldx $00                 ; 0->10 ; $00->$0a
loop:  
    lda title, x
    sta PPUDATA
    inx
    cpx #$0a
    bne loop
;;;;;;;;;;;;;;;;;;;;;;;;


    lda #$00
    sta PPUSCROLL
    sta PPUSCROLL

    


main:
    jmp main

nmi:
    rti

irq:
    rti


.segment "VECTORS"
    .word nmi                   ; nmi
    .word reset                 ; $0000, первая точка после запуска
    .word irq                   ; вторая точка после BRK

.segment "CHARS"
    .incbin "chr.bin"

.segment "RODATA"

title:
    .byte $10, $0a, $16, $0e, $00, $1d, $12, $1d, $15, $0e ; "GAME TITLE"

palette:
    .byte $2a, $30, $12, $0f;