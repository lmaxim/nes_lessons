;----------------------------------------
;
;   
;
;----------------------------------------


APU_FRAME_COUNTER   = $4017
APU_DMC_CTRL        = $4010        ; https://wiki.nesdev.com/w/index.php/APU_registers

PPU_CTRL            = $2000        ; https://wiki.nesdev.com/w/index.php/PPU_registers
PPU_MASK            = $2001
PPU_STATUS          = $2002

PPU_SCROLL          = $2005
PPU_ADDR            = $2006
PPU_DATA            = $2007

PPU_MASK_SHOW_BGR               = %00001000
PPU_CTRL_NMI_ON_SPRITES_PT0     = %10010000
PPU_CTRL_NMI_OFF_SPRITES_PT0    = %10010000