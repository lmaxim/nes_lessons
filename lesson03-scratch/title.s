.include "const.inc";

.export title_screen

; будем писать на экране нашими символами
.macro print_macro row, col, message, length
.local lbl_load
    lda row
    sta PPU_ADDR
    lda col
    sta PPU_ADDR

    ldx #$00                        
lbl_load:
    lda message, x
    sta PPU_DATA
    inx
    cpx length                      ; if (x != length)
    bne lbl_load                    ; then lbl_load
.endmacro

.segment "CODE"

.proc title_screen
    lda #PPU_CTRL_NMI_OFF_SPRITES_PT0
    sta PPU_CTRL

    print_macro #$21, #$ca, game_title, #$0b
    print_macro #$22, #$0a, press_start_message, #$0b

    lda $00
    sta PPU_SCROLL                  ; scrollX
    sta PPU_SCROLL                  ; scrollY

    lda #PPU_CTRL_NMI_ON_SPRITES_PT0
    sta PPU_CTRL

    rts
.endproc

.segment "RODATA"
press_start_message:
    .byte $19, $1b, $0e, $1c, $1c     ; PRESS
    .byte $00                         ; ' '
    .byte $1c, $1d, $0a, $1b, $1d     ; START

game_title:
    .byte $10, $0a, $16, $0e         ; GAME
    .byte $00                        ; ' '
    .byte $1d, $12, $1d, $15, $0e    ; TITLE