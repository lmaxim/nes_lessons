.include "const.inc";

.import main
.export setup

.macro ppu_load_page_macro hibyte, lobyte, pointer, lenght
.local lbl_load
    lda hibyte
    sta PPU_ADDR
    lda lobyte
    sta PPU_ADDR

    ldx #$00                        
lbl_load:
    lda pointer, x
    sta PPU_DATA
    inx
    cpx lenght                      ; if (x != lenght)
    bne lbl_load                    ; then lbl_load
.endmacro

.segment "CODE"
setup:
    ; установим палитры
    ; https://wiki.nesdev.com/w/index.php/PPU_palettes
    ; адрес $3f00
    ppu_load_page_macro #$3f, #$00, palette, #$20

    lda #PPU_CTRL_NMI_ON_SPRITES_PT0
    sta PPU_CTRL

    lda #PPU_MASK_SHOW_BGR
    sta PPU_MASK

    jmp main

.segment "RODATA"
palette:
    .byte $21, $30, $35, $3f    ; bgr 1  
    .byte $21, $13, $12, $11    ; bgr 2
    .byte $21, $23, $22, $21    ; bgr 3
    .byte $21, $33, $32, $31    ; bgr 4
    
    .byte $21, $35, $30, $3f    ; spr 1
    .byte $21, $35, $30, $3f    ; spr 2
    .byte $21, $35, $30, $3f    ; spr 3
    .byte $21, $3f, $35, $30    ; spr 4