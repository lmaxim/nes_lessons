.include "const.inc"

.import reset, title_screen
.export main

.segment "ZEROPAGE"

wait: .res 1
level: .res 1
update_ptr: .res 2

.segment "HEADER"
    .byte $4E, $45, $53, $1A    ; NES\0
    .byte $02                   ; PRG 2x16 = 32kb
    .byte $01                   ; CHR 1x8  = 8kb
    .byte %00000000             ; NROM-256
    .byte %00000000

.segment "CODE"

main:
    ; process_input
    ; update (ai/physics)
    ; render
    jsr title_screen

    lda #$01
    sta wait
@wait:
    bit wait
    bpl @wait

    jmp main

; render / music
nmi:
    lda #$00
    sta wait

    rti

irq:
    rti

.segment "VECTORS"
    .word nmi
    .word reset
    .word irq

.segment "CHARS"
    .incbin "lesson03.chr"
