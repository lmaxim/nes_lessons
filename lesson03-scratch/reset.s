.include "const.inc";

.import setup
.export reset

.segment "STARTUP"
reset:
    sei
    cld
    
    ; отключаем прерывание APU IRQ / BRK
    ldx #%01000000                 
    stx APU_FRAME_COUNTER

    ldx #$ff
    ; устанавливаем указатель стека на вершину адрес $01ff
    txs
    
    ; ff + 1 => 00, отключаем PPU и APU
    inx
    stx PPU_CTRL
    stx PPU_MASK
    stx APU_DMC_CTRL

    ; cбрасываем флаг статуса, чтобы отследить его изменение
    bit PPU_STATUS
@vblank1:
    bit PPU_STATUS
    bpl @vblank1

    ; в x все еще 0, a = 0, x = 1,2,3...ff
    txa
@clrmem:
    sta $000,x
    sta $100,x
    sta $200,x
    sta $300,x
    sta $400,x
    sta $500,x
    sta $600,x
    sta $700,x
    inx
    bne @clrmem

@vblank2:
    bit PPU_STATUS
    bpl @vblank2

    ; все подготовил, почистил. давай настраивай, как душа твоя захочет
    jmp setup